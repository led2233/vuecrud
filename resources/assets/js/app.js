new Vue({
    el: "#crud",
    created: function(){
        this.getKeeps();
    },
    data: {
        keeps: [],
        pagination:{
            'total': 0,
            'current_page': 0,
            'per_page': 0,
            'last_page': 0,
            'from': 0,
            'to': 0
        },
        newtask: '',
        filltask: {'id':'', 'task':''},
        errors: [],
        offset: 3,
    },
    computed: {
		isActived: function() {
			return this.pagination.current_page;
		},
		pagesNumber: function() {
			if(!this.pagination.to){
				return [];
			}

			var from = this.pagination.current_page - this.offset; 
			if(from < 1){
				from = 1;
			}

			var to = from + (this.offset * 2); 
			if(to >= this.pagination.last_page){
				to = this.pagination.last_page;
            }
            
			var pagesArray = [];
			while(from <= to){
				pagesArray.push(from);
				from++;
			}
			return pagesArray;
		}
	},
    methods: {
        getKeeps: function(page){
            var urlKeeps = 'tasks?page='+page;
            axios.get(urlKeeps).then(response => {
                this.keeps = response.data.task.data,
                this.pagination = response.data.pagination
            });
        },
        deleteKeep: function(keep) {
            var url = 'tasks/' + keep.id;
            axios.delete(url).then(response => {
                this.getKeeps();//listamos
                toastr.success('Eliminado correctamente');
            });
        },
        editkeep: function(keep){
            this.filltask.id = keep.id;
            this.filltask.task = keep.task;
            $('#edit').modal('show');
        },
        updatekeep: function(id){
            var url ='tasks/' + id;
            axios.put(url, this.filltask).then(response=>{
                this.getKeeps();
                this.filltask = {'id':'', 'task':''};
                this.errors = [];
                $('#edit').modal('hide');
                toastr.success('Tarea actualizada con éxito');
            }).catch(error => {
                this.errors = error.response.data
            })
        },
        createKeep: function() {
            var url = 'tasks';
            axios.post(url, {
               task: this.newtask  
            }).then(response => {
                this.getKeeps();
                this.newtask = '';
                this.errors = [];
                $('#create').modal('hide');
                toastr.success('Se creo una nueva tarea con éxito');
            }).catch(error => {
                this.errors = error.response.data
            });
        },
        changePage: function(page) {
			this.pagination.current_page = page;
			this.getKeeps(page);
		}
    }
});